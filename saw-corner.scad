//Drilling corners without hands wobbling: corner beam stand
// © 2019 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=54;
bs=50;        //beam side
bd=bs+1.41;   //beam diagonal
offs=20;      //top offset
thi=3;        //wall thickness */
wid=30;       //part width
hbd=bd/2;     //half of beam diagonal
k=(offs-thi/2)/hbd;  //k

//corner
 difference(){
     linear_extrude(height=wid){
          polygon([[0,0],[offs,hbd+thi],[hbd+offs,thi],[bd+offs,hbd+thi],[bd,0]]);
     }
     translate([0,0,-1]){
     linear_extrude(height=wid+2){
          polygon([[thi+thi*k,thi],[thi+offs,hbd-thi-thi*k],[hbd+offs-thi,thi]]);
     }
     linear_extrude(height=wid+2){
          polygon([[hbd+2*thi+(hbd-thi)*k,thi],[bd-thi+hbd*k,hbd-thi-thi*k],[bd-thi+thi*k,thi]]);
     }
     }
}
