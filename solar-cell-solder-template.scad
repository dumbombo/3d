//Keep soldering solar cell and conductive filament
//arranged correctly
// © 2020 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=50;

//input data
cell_width=52; //brush diameter
cell_length=26; //total part height
fg=0.4; //gap between parts and template
cell_thick=0.4; //cell thickness
wall=3; //wall thickness
film_width=2; //film width
film_length=26; //film length left aside solar cell
hole=41; //extracting hole diameter


difference(){
     //base
     union(){
          cube([cell_width+2*(fg+wall),cell_length+2*(wall+fg),wall+cell_thick]);
          translate([cell_width/2-film_width/2,-1*film_length,0]){
               cube([film_width+(wall+fg)*2,wall+film_length,wall+cell_thick]);
          }
          translate([cell_width/2+wall+fg,0,0]){
               cube([hole,hole,wall*2],center=true);
          }
     }

     // cuts
     // cell cut
     translate([wall,wall,wall]){
          cube([cell_width+2*fg,cell_length+2*fg,wall]);
     }
     // extraction hole cuts
     translate([cell_width/2+wall,cell_length+2*(fg+wall),0]){
          //cylinder(d=hole,h=wall*2);
          cube([hole/10,hole*2,hole],center=true);
     }
     translate([cell_width+2*(fg+wall),0,0]){
          //cylinder(d=hole,h=wall*2);
          cube([hole/2,hole,hole],center=true);
     }
     // cylinder(d=hole,h=wall*2);
     cube([hole/2,hole,hole],center=true);
     // film cut
     translate([cell_width/2+wall+fg-film_width/2,-1*film_length,wall]){
          cube([film_width+fg,film_length+wall*2,wall]);
     }
     //final bottom cut
     translate([-100,-100,-1*wall]){
          cube([200,200,wall]);
     }
};
