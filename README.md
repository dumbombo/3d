3d
==

Public funny and maybe useful 3d stuff for Thingiverse

List of project in this repo
----------------------------

- drill guide for corner drilling
- knob for home water boiler Baxi Eco
- front wheel for robotic vacuum cleaner
- hub of DIY brush for robotic vacuum cleaner
- extending arm for car backup camera
- frame of box for HDDs
- jar cover converting jar to illuminated aquarium
- flat table base for articulated lamp
- hook for luggage compartment cover for VW Golf IV
- hook for strap for anti-covid mask
- round meshed cap for dynamic microphone
- holder for mobile phone
- pulling rod for rear door lock of VW Golf IV
- .....
- table holder for screwdrivers
- plastic block for placing conductive film on solar cell before soldering
- piece of plastic needed to place covid mask straps around the head
- holder for sharpening whetstone intended to place the stone on the table

Licensed under cc-by-sa
