// Drill guide parameters
// © 2019 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=54;
left=70;   //left side length
right=70;  //right side length
hei=18;    //width
thi=3;     //thickness
fra=1.5;   //hole frame thickness
dia=7;     //drilling diameter
pt=0.1;    //parts tolerance

//sphere(r=left);
