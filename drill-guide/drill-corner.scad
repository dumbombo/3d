//Drilling corners without hands wobbling: corner frame
// © 2019 MadBot
//Licensed under the CC-BY-SA

/* $fs=0.1; */
/* $fn=54; */
/* left=70;   //left side length */
/* right=70;  //right side length */
/* hei=18;    //width */
/* thi=3;     //thickness */
/* fra=1.5;   //hole frame thickness */
/* dia=7;     //drilling diameter */
/* pt=0.1;   //parts tolerance */

include <drill-parameters.scad>;

//corner
 difference(){
     linear_extrude(height=hei){
          polygon([[0,0],[left,0],[left,thi],[thi,thi],[thi,right],[0,right]]);
     }
     translate([-0.1,-0.1,fra*2-pt]){
          cube([hei-fra+pt,hei-fra+pt,hei-fra*4+pt*2]);
     }
     translate([fra,fra,fra]){
          cube([hei-fra,hei-fra,hei-fra*2+pt]);
     }

}
