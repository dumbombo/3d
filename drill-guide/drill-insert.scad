//Drilling corners without hands wobbling: insert
// © 2019 MadBot
//Licensed under the CC-BY-SA

//$fs=0.1;
/* $fn=54; */
/* left=70;   //left side length */
/* right=70;  //right side length */
/* hei=18;    //width */
/* thi=3;     //thickness */
/* fra=1.5;   //hole frame thickness */
/* dia=7;     //drilling diameter */
/* pt=0.1;   //parts tolerance */

//guiding insert

     difference(){
          union(){
               translate([-0.1,-0.1,fra]){
                    cube([hei-fra,hei-fra,hei-fra*4]);
               }
               translate([fra,fra,0]){
                    cube([hei-fra,hei-fra,hei-fra*2]);
               }
               translate([dia/2,dia/2,hei/2-fra]){
                    rotate([90,0,-45]){
                         cylinder(d=dia+thi,h=dia*3);
                    }
               }
          }
          translate([thi,thi,0]){
               cube([hei,hei,hei]);
          }
          translate([dia,dia,hei/2-fra]){
               rotate([90,0,-45]){
                    cylinder(d=dia,h=dia*3+dia+0.1);
               }
          }
     }


