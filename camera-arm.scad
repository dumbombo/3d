//Backup car camera holding arm
// © 2018 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=20;
thi=3;
wid=19.5;
hei=10;
hole=2.2;
et=60;

difference(){
    union(){
        cube([thi,et-hei/2,hei]);
        translate([0,et-hei/2,hei/2])
        rotate([0,90,0])
        cylinder(r=hei/2,h=thi);
        translate([wid,0,0])
        cube([thi,et-hei/2,hei]);
        translate([wid,et-hei/2,hei/2])
        rotate([0,90,0])
        cylinder(r=hei/2,h=thi);
        cube([wid,thi,hei]);        
        translate([0,et/2,0])
        cube([wid,thi,hei]);
    }
        translate([-5,thi*5,hei/2])
        rotate([0,90,0]) 
        cylinder(r=hole/2,h=wid*2);
        translate([-5,et-hei/2,hei/2])
        rotate([0,90,0]) 
        cylinder(r=hole/2,h=wid*2);
    
}

translate([wid/2+thi/2,0,hei/3])
rotate([90,0.0])
linear_extrude(height=thi/4){
    text("Mad Bot",4.24,halign="center");
}
