//Universal screwdriver holder
// © 2018 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=128;
dia=20; //base cylinder diameter
length=100; //base cylinder length
hole_count=6; //count of holes for smallest screwdrivers

//hull
difference(){
    cylinder(d=dia,h=length);
    translate([-1*length,0,0]){
        cube(size=[length*2,length*2,length*2]);
    };

    difference(){
//        cylinder(d=dia-4,h=length);
        translate([-1*length,0,0]){
            cube(size=[length*2,length*2,length*2]);
        };
        translate([dia/16,-1*dia-10,0]){
            cube(size=[length*2,length*2,length*2]);
        };
    };
    translate([dia/16,-1*dia,0]){
        cube(size=[length*2,length*2,length*2]);
    };
    //holes
    for(i=[1:3]){
        rotate([i*26,-90,0]){
            interval=length/(hole_count-i+2);
            translate([(-1/(hole_count-i+2))*interval,0,0]){
                for(j=[1:hole_count-i+1]){
                    new_interval=(length-interval)/(hole_count-i);
                    translate([new_interval*(j)-new_interval*0.5,0,0]){


 //                           cylinder(r=3*i/2,h=(dia/2+1));

                    };
                };
            };
        };
     };

};

//pipes
difference(){
    for(i=[1:3]){
        rotate([i*26-15,-90,0]){
            interval=length/(hole_count-i+2);
            translate([(-1/(hole_count-i+2))*interval,0,0]){
                for(j=[1:hole_count-i+1]){
                    new_interval=(length-interval)/(hole_count-i);
                    translate([new_interval*(j)-new_interval*0.5,0,0]){
                        difference(){
                            cylinder(r=3*i/2+1,h=(dia*2-1));
                            cylinder(r=3*i/2,h=(dia*2));
                        }
                    };
                };
            };
        };
     };
     cylinder(d=dia,h=length);
}

//shelf
difference(){
    translate([-1*length+dia/16,0,0]){
        cube(size=[length,6,length]);
    };
    translate([-1*length+3,-0.2,3]){
         cube(size=[length-(dia*2),1.5,length-6]);
    };
};
