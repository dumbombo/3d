//Universal rotating knob made for Baxi boiler
// © 2018 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=54;
dia=36; //head diameter
thi=1.5;  //wall thickness
shl=25;  //handle shaft length
sh_dia=10; //handle shaft diameter
hea=18; //head height


//head
difference(){
    //body
    cylinder(r=dia/2,h=hea);
    translate([0,0,5])
    cylinder(r=dia/2-thi,h=hea);
    //handle
    difference(){

        cylinder(r=dia/2-thi*3,h=3);
        translate([-1*dia/2,-2,0])
        cube([dia,4,3]);
    }
}

//shaft
translate([0,0,5]){
    difference(){
        cylinder(r=sh_dia/2,h=shl);
        difference(){
            cylinder(d=sh_dia-thi*2,h=shl);
            translate([1.8,-sh_dia/2,0])
            cube([sh_dia,sh_dia,shl]);
        }
    }
}
