//Replace chinese whetstone sharpener
//using own table fixer
// © 2019 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=50;

//input data
h_dia=20; //hole diameter
h_depth=50; //hole depth
length=130; //total part height
height=20; //total part width
s_length=120; //stone length
