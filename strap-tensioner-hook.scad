//Fix mask straps on the nape
//using hook sticked to strap tensioner
// © 2020 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=50;

//input data
length=40; //part length
width=35; //part width
thcks=4; //part thickness
hk_gap=2; //hook gape
hk_bit=20; //hook bite
hole_wi=28;
hole_le=12.7;
fi=3; //motor shaft interface height


minkowski(){
     difference(){
          union(){
               cube([length-fi,width-fi,thcks-fi]);
               cube([thcks-fi,width-fi,thcks+hk_gap]);
               translate([0,0,thcks+hk_gap]){
                    cube([hk_bit,width-fi,thcks-fi]);
               }
          }
          translate([length-hole_le-fi,(width-hole_wi)/2-fi/2,0]){
               cube([thcks,hole_wi,fi]);
          }
          translate([length-hole_le+thcks*2-fi,(width-hole_wi)/2-fi/2,0]){
               cube([thcks,hole_wi,fi]);
          }

     }

     sphere(d=fi);
}
