//Fix mask straps on the mask
//using stitched hook
// © 2020 MadBot
//Licensed under the CC-BY-SA

$fs=0.9;
$fn=20;

//input data
length=10; //main part length
width=35; //part width
thcks=4; //part thickness
hk_len=15; //hook length
hk_gap=10; //hook gape
hk_bit=5; //hook bite
hole_wi=30;
hole_le=2;
fi=3; //motor shaft interface height

minkowski(){
     difference(){
          union(){
               cube([length-fi,width-fi,thcks-fi]);
               translate([length-fi,width/2-hk_bit/2,0]){
                    cube([hk_len,hk_bit-fi,thcks-fi]);
               }
          }
          translate([thcks-fi,(width-hole_wi)/2-fi/2,0]){
               cube([thcks,hole_wi,fi]);
          }


     }

     sphere(d=fi);
}
