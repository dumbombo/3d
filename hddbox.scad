//universal hdd stand
// © 2018 MadBot
//Licensed under the CC-BY-SA


$fs=0.1;
$fn=60;

cell_count=4;
cell_width=100;
cell_length=25;
cell_height=25;
wall_th=2;

//wide wall
for(i=[0:cell_count]){
    translate([0,i*cell_length,0]){
        cube([cell_width,wall_th,cell_height]);
    }
}
//long walls
translate([-1*wall_th,0,0]){
    cube([wall_th,cell_length*cell_count+wall_th,cell_height]);
}
translate([cell_width,0,0]){
    cube([wall_th,cell_length*cell_count+wall_th,cell_height]);
}
//floor beams
for(i=[1:8]){
     translate([i*cell_width/8,0,0]){
         cube([wall_th,cell_length*cell_count+wall_th,wall_th]);
     }
}

//feet
//still incomplete
