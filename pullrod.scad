//pulling rod for rear door lock of VW mk4
// © 2018 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=54;
dia=9.44; //head diameter
thi=1.6;  //wall thickness
rl=25.4;  //rod length
r_width=6; //rod width
r_height=4; //rod height


difference(){
    union(){
        //rod
        difference(){
            cube([rl,r_width,r_height]);
            translate([-3,3,2]){
                sphere(d=dia-thi);
            }
            translate([rl+3,3,2]){
                sphere(d=dia-thi);
            }

        }
        //head #1
        difference(){
            translate([-3,3,2]){
                difference(){
                    sphere(d=dia);
                    sphere(d=dia-thi);
                }
            }
            translate([-dia,-1*r_width,-rl]){
                                cube([rl,rl,rl]);
            }
        }
        //head #2
        difference(){
            translate([rl+3,3,r_height-2]){
                difference(){
                    sphere(d=dia);
                    sphere(d=dia-thi);
                }
            }
            translate([rl-dia,-1*r_width,r_height]){
                                cube([rl,rl,rl]);
            }
        }
    }

}
