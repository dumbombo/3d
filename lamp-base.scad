//Articulated lamp base plate
// © 2018 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=54;
dia=180; //plate diameter
hei=12.7; //head height
thi=1.5;  //wall thickness
shl=25;  //handle shaft length
shd_ia=10; //handle shaft diameter



//difference(){
    cylinder(r=dia/2,h=hei);
    translate([0,300,-300]);
    sphere(r=dia/2);
//}
