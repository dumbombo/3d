//luggage shelf door pulling hook for volkswagen mk4
// © 2019 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=54;
max_dia=11; //hook width
min_dia=5; //knot side width
hole_dia=3; //diameter of the wire hole
thi=1.6;  //wall thickness
hl=28.37;  //hook length


difference(){
//common shape
    hull(){
        sphere(d=max_dia);
        translate([hl-max_dia/2-min_dia/2,0,0]){
            cube(min_dia,center=true);
        }
    }

//cut lower stuff
    translate([-1*hl,-1*hl,thi]){
       cube([hl*3,hl*3,hl]);
    }

//hook hole
    translate([0,0,thi]){
        sphere(d=max_dia-thi);
    }

//wire hole
    translate([0,0,-1*(min_dia-hole_dia)/4]){
        rotate([0,90,0]){
            cylinder(d=hole_dia,h=hl);
        }
    }
//knot hole
    rotate([0,90,0]){
         cylinder(d=min_dia,h=min_dia);
    }

}

//hook hole edge
translate([0,0,thi/2.]){
    difference() {
        cylinder(h=thi/2, d=max_dia);
        cylinder(h=thi/2, d=max_dia-thi*2);
    }
}
