//Replacement cap for broken microphone
// © 2018 MadBot
//Licensed under the CC-BY-SA


$fs=0.1;
$fn=60;

ring_do=53;
ring_di=47;
ring_h=3.0;
iring_do=49;
iring_di=47;
cap_do=51;
caph=52;
holed=3.0;




difference(){
    sphere(d=ring_di+1);
    sphere(d=ring_di);
    translate([-1*ring_do,-1*ring_do,-2*ring_do]){
        cube(size=ring_do*2);
    }

    for(j=[1:7]){
        k=6*j;
        for(i=[0:k]){
             translate([3.5*j*cos(i*(360/k)),3.5*j*sin(i*(360/k)),0]){
                cylinder(d=holed-j*0.1,h=100);
             }
        }
    }
    cylinder(d=ring_h,h=caph);
}

difference(){
    cylinder(d=ring_do,h=ring_h);
    cylinder(d=ring_di,h=ring_h);
}
translate([0,0,ring_h/-2]){
    difference(){
        cylinder(d=iring_do,h=ring_h);
        cylinder(d=iring_di,h=ring_h);
    }
}
