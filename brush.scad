//Replace brushes of chinese robotic vacuum cleaner
//using own brush hubs and some brush bristles
// © 2019 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=50;

//input data
br_dia=23; //brush diameter
total_height=13; //total part height
bp_int_height=3; //bottom part interface height
scr_hol_dia=4; //screw hole diameter
sf_int_dia=12; //motor shaft interface diameter
sf_int_height=3; //motor shaft interface height
sf_dia=7.5;//shaft diameter
bri_dia=3.5; //bristle diameter

//geometric data
br_height=total_height-bp_int_height; //brush cap height
sph_rad=sqrt(pow(((pow(br_height,2)-pow(br_dia,2))/(2*br_height)),2))/2; //sphere radius
cap_dia=scr_hol_dia*2; //screw cap diameter


//sphere segment with a screw hole, bristle holes and motor interface volume cut
difference(){
     sphere(r=sph_rad);
     //screw hole
     translate([0,0,-2*sph_rad]){
          cylinder(d=scr_hol_dia,h=5*sph_rad);
     };
     //screw cap hole
     translate([0,0,(sph_rad-br_height/2)]){
          cylinder(d=cap_dia,h=cap_dia*2);
     };
     //cut sphere leaving top segment
     translate([0,0,(-sph_rad-br_height)]){
          cylinder(r=sph_rad*2,h=sph_rad*2);
     };
     //cut bristle L-shape holes
     for (i=[0,180]){
          rotate([-20,20,i]){
               translate([-sph_rad/2-bri_dia,-sph_rad/2-bri_dia,(sph_rad-br_height*0.9)]){
                    rotate_extrude(convexity = 20, angle=130){
                         translate([sph_rad/2, 0, 0]){
                              circle(d=bri_dia);
                         };
                    };
               };
               translate([0-bri_dia,-sph_rad/2-bri_dia,(sph_rad-br_height*0.9)]){
                    rotate([90,0,0]){
                         cylinder(d=bri_dia,h=br_dia);
                    };
               };
               translate([-sph_rad/2-bri_dia,0-bri_dia,(sph_rad-br_height*0.9)]){
                    rotate([0,-90,0]){
                         cylinder(d=bri_dia,h=br_dia);
                    };
               };
          };
     };
     //cut motor shaft interface volume inside cap
     cylinder(d=sf_int_dia,h=(sph_rad-br_height)+sf_int_height);
};

//motor shaft
difference(){
     //cylinder filling interface volume inside cap
     cylinder(d=sf_int_dia,h=(sph_rad-br_height)+sf_int_height);

     //double D spline
     difference(){
          cylinder(d=sf_dia,h=sf_int_dia);
          for (i=[0,180]){
               rotate([0,0,i]){
                    translate([0.8*sf_dia/2,-sf_dia,0]){
                         cube([sf_int_dia,sf_int_dia,sf_int_dia]);
                    };
               };
          };
     };

     //polygonal spline
     /* linear_extrude(height=sf_int_dia,convexity=10){ */
     /*      order=6; */
     /*      angles=[ for (i = [0:order-1]) i*(360/order) ]; */
     /*      coords=[ for (th=angles) [sf_dia/2*cos(th), sf_dia/2*sin(th)] ]; */
     /*      polygon(coords); */
     /* }; */

     //final cut
     translate([0,0,-1]){
          cylinder(r=sph_rad,h=sph_rad-total_height+1);
     }
};
