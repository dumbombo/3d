//Liectroux x5s front wheel
// © 2019 MadBot
//Licensed under the CC-BY-SA

$fs=0.1;
$fn=54;
dia=24; //wheel diameter
wid=20; //wheel width
fir=5;  //round fillet radius
axd=4.1;  //axle diameter



difference(){
     //rounded base cylinder
     minkowski(){
          cylinder(d=dia-fir*2,h=wid-fir*2);
          sphere(r=fir);
     }
     //axle hole
     translate([0,0,-1*fir]){
          cylinder(h=wid+fir,d=axd);
     }
     //some madskillz
     for (i=[-1*fir,wid-fir]){
          translate([0,0,i]){
               rotate_extrude(){
                    translate([axd*1.5,0,0]){
                         circle(d=axd*1.2);
                    }
               }
          }
     }    
}
